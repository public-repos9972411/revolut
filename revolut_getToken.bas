' Module: get Access Token using refresh token
Option Compare Database
Option Explicit
Global Const REVOLUT_BASE_URL As String = "https://sandbox-b2b.revolut.com"
Private RevolutstrCredentialsType As String
Private RevolutstrRefresh_token As String
Private RevolutstrAssertionType As String
Private RevolutstrJWT As String
Private RevolutstrClientId As String
Public RevolutToken As String
Public TokenExpiry As Date

' Modified RCreateToken with error handling
Public Function RevolutCreateToken() As String
    Dim xhr As Object
    Dim expiresIn As Long
    
    On Error GoTo ErrorHandler

    ' set up the payload
    RevolutstrCredentialsType = "refresh_token" ' credentials type
    RevolutstrRefresh_token = "oa_sand_gzA0Zt9999999lalalalala" 'refresh token from enable app step
    RevolutstrClientId = "s0g1Z4z5vH5zaTsxiAaoaooaoaoaooa" 'client id
    RevolutstrAssertionType = "urn:ietf:params:oauth:client-assertion-type:jwt-bearer" ' assertions
    ' jwt generated from privately obtained certificate
    RevolutstrJWT = "eyAgICAiYWxnIjogIl.akakkaklalla.oppqpqpqppq"
    
    ' set endppoint url
    Dim RevolutURL As String
    RevolutURL = REVOLUT_BASE_URL + "/api/1.0/auth/token"

    ' set the http object
    Set xhr = CreateObject("MSXML2.ServerXMLHTTP.6.0")

    With xhr
        .Open "POST", RevolutURL, False
        .setRequestHeader "Accept", "application/json"
        .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
        .send RevolutCreateTokenRequest()
    End With

    ' parse the response
    Dim RevolutjsonText As String
    RevolutjsonText = xhr.ResponseText

    Dim Revolutjson As Object
    Set Revolutjson = JsonConverter.ParseJson(RevolutjsonText)

    ' extract expire time
    expiresIn = Revolutjson("expires_in")
    TokenExpiry = Now + TimeSerial(0, expiresIn / 60, 0)
    
    ' extract the token
    RevolutCreateToken = Revolutjson("access_token")

    'clean up
    Set xhr = Nothing
    
    Exit Function

ErrorHandler:
    ' Handle specific error, or just reset values and raise error to caller.
    RevolutToken = ""
    TokenExpiry = 0
    Err.Raise Err.Number, Err.Source, "Error fetching token: " & Err.Description, Err.HelpFile, Err.HelpContext

End Function

Public Function GetToken() As String
    ' If token is empty or expired, fetch a new one
    If RevolutToken = "" Or TokenExpiry <= Now Then
        RevolutToken = RevolutCreateToken()
    End If
    
    GetToken = RevolutToken

End Function

Function RevolutCreateTokenRequest() As String
    '// Create Token payload
    RevolutCreateTokenRequest = RevolutCreateTokenRequest & "&grant_type=" & RevolutstrCredentialsType
    RevolutCreateTokenRequest = RevolutCreateTokenRequest & "&refresh_token=" & RevolutstrRefresh_token
    RevolutCreateTokenRequest = RevolutCreateTokenRequest & "&client_assertion_type=" & RevolutstrAssertionType
    RevolutCreateTokenRequest = RevolutCreateTokenRequest & "&client_id=" & RevolutstrClientId
    RevolutCreateTokenRequest = RevolutCreateTokenRequest & "&client_assertion=" & RevolutstrJWT

'    Debug.Print RevolutCreateTokenRequest
 
End Function


