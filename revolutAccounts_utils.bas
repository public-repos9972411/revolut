Option Compare Database
Option Explicit

Public Function saveAccounts(ByRef RevolutjsonText As Variant) As String
        Dim revCount As Integer
        revCount = 0
        
        ' Initialize database and recordset
        ' Parse the JSON string
        Dim Accountsjson As Object
        Set Accountsjson = JsonConverter.ParseJson(RevolutjsonText)
    
        ' Set up the database and recordset objects
        Dim dbAcounts As DAO.Database
        Dim rsRevolutAccounts As DAO.Recordset
        Set dbAcounts = CurrentDb()
        Set rsRevolutAccounts = dbAcounts.OpenRecordset("RevolutAccounts", dbOpenDynaset)

        Dim item As Dictionary
    
        ' Loop through each item in the parsed JSON array
        For Each item In Accountsjson
            ' Check for existing record
            Dim strACriteria As String
            strACriteria = "id_ = '" & item("id") & "'"
            
            rsRevolutAccounts.FindFirst strACriteria
            
            If rsRevolutAccounts.NoMatch Then ' If no match found, then add new record
                With rsRevolutAccounts
                    .AddNew
                    ![id_] = item("id")
                    If item.Exists("name") Then ![name_] = item("name") Else ![name_] = Null ' Check if the name exists in the dictionary
                    ![balance] = item("balance")
                    ![currency] = item("currency")
                    ![State] = item("state")
                    ![Public] = item("public")
                    ![created_at] = item("created_at")
                    ![updated_at] = item("updated_at")
                    .Update
                    revCount = revCount + 1
                End With
            End If
            
        Next item
    
        ' Clean up
        rsRevolutAccounts.Close
        Set rsRevolutAccounts = Nothing
        Set dbAcounts = Nothing
        
        MsgBox ("Successfully pulled " & revCount & " accounts from Revolut")

End Function
