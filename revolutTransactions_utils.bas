Option Compare Database
Option Explicit
' +++++++++++++++++++++++++++++++++++++++++++
Public Type DataInfo
    from As String
    upto As String
    counterpartyid As String
    accountid As String
End Type

Public Function saveTransactions(ByRef Transactionsjson As Variant) As String
        Dim revCount As Integer
        revCount = 0
        
        Dim dbTransactions As DAO.Database
        Dim rsRevolutData As DAO.Recordset
        
        Set dbTransactions = CurrentDb
        Set rsRevolutData = dbTransactions.OpenRecordset("RevolutTransactions", dbOpenDynaset)

        Dim Transaction As Dictionary, Leg As Dictionary
        
        For Each Transaction In Transactionsjson
            ' Check for existing record
            Dim strTCriteria As String
            strTCriteria = "id_ = '" & Transaction("id") & "'"
            
            rsRevolutData.FindFirst strTCriteria
            
            If rsRevolutData.NoMatch Then ' If no match found, then add new record
                For Each Leg In Transaction("legs")
                    With rsRevolutData
                        .AddNew
                        ![id_] = Transaction("id")
                        ![Type] = Transaction("type")
                        ![State] = Transaction("state")
                        ![request_id] = Transaction("request_id")
                        ![created_at] = Transaction("created_at")
                        ![updated_at] = Transaction("updated_at")
                        If Transaction.Exists("reference") Then ![Reference] = Transaction("reference")
                        
                        ![leg_id] = Leg("leg_id")
                        ![leg_account_id] = Leg("account_id")
                        ![amount] = Leg("amount")
                        ![currency] = Leg("currency")
                        ![Description] = Leg("description")
                        If Leg.Exists("fee") Then ![fee] = Leg("fee")
                        If Leg.Exists("bill_amount") Then ![bill_amount] = Leg("bill_amount")
                        If Leg.Exists("bill_currency") Then ![bill_currency] = Leg("bill_currency")
                        If Leg.Exists("balance") Then ![balance] = Leg("balance")
                        
                        If Leg.Exists("counterparty") Then
                            ![counterparty_id] = Leg("counterparty")("id")
                            ![counterparty_account_type] = Leg("counterparty")("account_type")
                            ![counterparty_account_id] = Leg("counterparty")("account_id")
                        End If
                        
                        If Transaction.Exists("merchant") Then
                            ![merchant_name] = Transaction("merchant")("name")
                            ![merchant_city] = Transaction("merchant")("city")
                            ![merchant_category_code] = Transaction("merchant")("category_code")
                            ![merchant_country] = Transaction("merchant")("country")
                        End If
                        
                        If Transaction.Exists("card") Then
                            ![card_number] = Transaction("card")("card_number")
                        End If
                        
                        .Update
                        revCount = revCount + 1
                    End With
                Next Leg
            End If
            
        Next Transaction
        
        rsRevolutData.Close
        Set dbTransactions = Nothing
        
        MsgBox ("Successfully pulled " & revCount & " Transactions from Revolut")

End Function


Public Sub getDatafilters(ByRef Datafilters As DataInfo)
    
    Const FORM_NAME As String = "revolut_TRANSACTIONS"
    
    DoCmd.OpenForm FORM_NAME, , , , , acDialog
    
    If formIsOpen(FORM_NAME) Then
        With Forms(FORM_NAME)
            ' Get values from form
            Datafilters.from = getControlValue(.Controls("from"))
            Datafilters.upto = getControlValue(.Controls("upto"))
            Datafilters.counterpartyid = getControlValue(.Controls("counterpartyid"))
            Datafilters.accountid = getControlValue(.Controls("accountid"))

            ' Reset values
            resetControlValue .Controls("from")
            resetControlValue .Controls("upto")
            resetControlValue .Controls("counterpartyid")
            resetControlValue .Controls("accountid")
        End With
        
        DoCmd.Close acForm, FORM_NAME
    End If
End Sub

Private Function getControlValue(ctrl As Control) As String
    If IsNull(ctrl.Value) Then
        getControlValue = ""
    Else
        getControlValue = ctrl.Value
    End If
End Function

Private Sub resetControlValue(ctrl As Control)
    If Not IsNull(ctrl.Value) And Len(ctrl.Value) > 0 Then
        ctrl.Value = ""
    End If
End Sub

Private Function AddParameter(url As String, paramName As String, paramValue As String) As String
    If paramValue <> "" Then
        If InStr(url, "?") = 0 Then ' If no parameters have been added yet
            url = url & "?"
        Else
            url = url & "&"
        End If
        url = url & paramName & "=" & paramValue
    End If
    AddParameter = url
End Function

Public Function BuildURL(ByRef TransactionURL As String, ByRef Datafilters As DataInfo)
    
    TransactionURL = REVOLUT_BASE_URL & "/api/1.0/transactions"
    TransactionURL = AddParameter(TransactionURL, "from", Datafilters.from)
    TransactionURL = AddParameter(TransactionURL, "to", Datafilters.upto)
    TransactionURL = AddParameter(TransactionURL, "counterparty", Datafilters.counterpartyid)
    TransactionURL = AddParameter(TransactionURL, "account", Datafilters.accountid)
    BuildURL = TransactionURL
'    Debug.Print TransactionURL
End Function

