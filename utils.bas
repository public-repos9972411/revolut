' Module: Utils
Option Compare Database
Option Explicit

'Global N As Long, iCount As Long 'batchsize As Long
Public Function formIsOpen(ByVal formName As String) As Boolean
    'check if form is still open
    formIsOpen = SysCmd(acSysCmdGetObjectState, acForm, formName)

End Function

Function URLEncode(ByVal strText As String) As String
    Dim i As Integer
    Dim char As String
    Dim charCode As Integer

    URLEncode = ""

    For i = 1 To Len(strText)
        char = Mid(strText, i, 1)
        charCode = Asc(char)

        If (charCode >= 65 And charCode <= 90) Then ' A-Z
            URLEncode = URLEncode & char
        ElseIf (charCode >= 97 And charCode <= 122) Then ' a-z
            URLEncode = URLEncode & char
        ElseIf (charCode >= 48 And charCode <= 57) Then ' 0-9
            URLEncode = URLEncode & char
        ElseIf charCode = 45 Or charCode = 95 Or charCode = 46 Or charCode = 126 Then ' - _ . ~
            URLEncode = URLEncode & char
        Else
            URLEncode = URLEncode & "%" & Right("0" & Hex(charCode), 2)
        End If
    Next i
    
End Function

Function BytesToBase64Url(byteArray() As Byte) As String
    Dim base64String As String
    base64String = BytesToBase64(byteArray) ' Using the previous function

    ' Convert Base64 to Base64Url
    base64String = Replace(base64String, "+", "-")
    base64String = Replace(base64String, "/", "_")
    base64String = Replace(base64String, "=", "")

    BytesToBase64Url = base64String
End Function

Function BytesToBase64(byteArray() As Byte) As String
    Dim xmlObj As Object
    Dim node As Object

    ' Convert bytes to Base64 using XMLDOM
    Set xmlObj = CreateObject("MSXML2.DOMDocument")
    Set node = xmlObj.createElement("base64")
    node.DataType = "bin.base64"
    node.nodeTypedValue = byteArray
    BytesToBase64 = node.Text

    ' Cleanup
    Set node = Nothing
    Set xmlObj = Nothing
End Function

Function StringToBytes(inputString As String) As Byte()
    StringToBytes = StrConv(inputString, vbFromUnicode)
End Function

' This function checks if an error description indicates an invalid token
Function IsTokenError(errDesc As String) As Boolean
    ' Here you'll check for known error descriptions or error codes that indicate a token issue.
    ' This is a very basic example; modify based on your actual error messages or codes.
    If InStr(1, errDesc, "Unexpected error", vbTextCompare) > 0 Then
        IsTokenError = True
    Else
        IsTokenError = False
    End If
End Function

