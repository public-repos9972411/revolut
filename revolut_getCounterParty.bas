Option Compare Database
Option Explicit
        
Sub getCounterparty()
    Dim xhr As Object
    Dim json As Object
    Dim RetryCount As Integer
    Dim MaxRetries As Integer: MaxRetries = 1
    
    ' handdle any token error requests
    On Error GoTo ErrorHandler
    
StartCall:
    ' get the token
    RevolutToken = GetToken()
 
    ' set up the URI
    Dim RevolutCounterURL As String
    RevolutCounterURL = REVOLUT_BASE_URL + "/api/1.0/counterparties"
    
    ' set the http object
    Set xhr = CreateObject("MSXML2.ServerXMLHTTP.6.0")
    
    With xhr
        .Open "GET", RevolutCounterURL, False
        .setRequestHeader "Accept", "application/json"
        .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
        .setRequestHeader "Authorization", "Bearer " + RevolutToken
        .send
        
        If .Status = 200 Or .Status = 204 Or .Status = 201 Then
        
            ' parse the response
            Dim RevolutjsonText As String
            RevolutjsonText = .ResponseText
    
            Dim Revolutjson As Object
            Set Revolutjson = JsonConverter.ParseJson(RevolutjsonText)
            
            ' Initialize database and recordset
            Dim item As Dictionary
            Dim Account As Dictionary
            
            Dim rsRevolutCounterParty As DAO.Recordset
            Dim rsUKCounterParty As DAO.Recordset
            Dim rsINTCounterParty As DAO.Recordset
            
            Set rsRevolutCounterParty = CurrentDb.OpenRecordset("RevolutCounterParty", dbOpenDynaset)
            Set rsUKCounterParty = CurrentDb.OpenRecordset("UKCounterParty", dbOpenDynaset)
            Set rsINTCounterParty = CurrentDb.OpenRecordset("INTCounterParty", dbOpenDynaset)
            

            For Each item In Revolutjson
                ' Check for "profile_type"
                If item.Exists("profile_type") Then
                
                    ' Check for existing record
                    Dim strCriteria As String
                    strCriteria = "id_ = '" & item("id") & "'"
                    
                    rsRevolutCounterParty.FindFirst strCriteria
                    
                    If rsRevolutCounterParty.NoMatch Then ' If no match found, then add new record
                        With rsRevolutCounterParty
                            .AddNew
                            ![id_] = item("id")
                            ![name_] = item("name")
                            ![revtag] = item("revtag")
                            ![profile_type] = item("profile_type")
                            ![country] = item("country")
                            ![State] = item("state")
                            ![created_at] = item("created_at")
                            ![updated_at] = item("updated_at")
                            .Update
                        End With
                    End If
                End If
                
                ' Check for "accounts"
                If item.Exists("accounts") Then
                    For Each Account In item("accounts")
                        ' For "bank_country": "GB"
                        If Account("bank_country") = "GB" And Not Account.Exists("iban") And Not Account.Exists("bic") Then
                        
                            ' Check for existing record
                            Dim str2Criteria As String
                            str2Criteria = "id_ = '" & item("id") & "'"
                            
                            rsUKCounterParty.FindFirst str2Criteria
                            
                            If rsUKCounterParty.NoMatch Then ' If no match found, then add new record
                              With rsUKCounterParty
                                    .AddNew
                                    ![id_] = item("id")
                                    ![name_] = item("name")
                                    ![State] = item("state")
                                    ![created_at] = item("created_at")
                                    ![updated_at] = item("updated_at")
                                    ![account_no] = Account("account_no")
                                    ![sort_code] = Account("sort_code")
                                    ![account_id] = Account("id")
                                    ![Type] = Account("type")
                                    ![account_name] = Account("name")
                                    ![bank_country] = Account("bank_country")
                                    ![currency] = Account("currency")
                                    ![recipient_charges] = Account("recipient_charges")
                                    .Update
                                End With
                            End If
                            
                        ' For accounts with "iban" or "bic"
                        ElseIf Account.Exists("iban") Or Account.Exists("bic") Then
                            
                            ' Check for existing record
                            Dim str3Criteria As String
                            str3Criteria = "id_ = '" & item("id") & "'"
                            
                            rsINTCounterParty.FindFirst str3Criteria
                            
                            If rsINTCounterParty.NoMatch Then ' If no match found, then add new record
                                With rsINTCounterParty
                                    .AddNew
                                    ![id_] = item("id")
                                    ![name_] = item("name")
                                    ![State] = item("state")
                                    ![created_at] = item("created_at")
                                    ![updated_at] = item("updated_at")
                                    ![iban] = Account("iban")
                                    ![bic] = Account("bic")
                                    ![account_id] = Account("id")
                                    ![Type] = Account("type")
                                    ![account_name] = Account("name")
                                    ![bank_country] = Account("bank_country")
                                    ![currency] = Account("currency")
                                    ![recipient_charges] = Account("recipient_charges")
                                    .Update
                                End With
                            End If
                        End If
                    Next Account
                End If
            Next item
            
            rsRevolutCounterParty.Close
            rsUKCounterParty.Close
            rsINTCounterParty.Close
            
            Set rsRevolutCounterParty = Nothing
            Set rsUKCounterParty = Nothing
            Set rsINTCounterParty = Nothing

        MsgBox ("Successfully pulled CounerParty from Revolut:")
             
        Else
            MsgBox ("Error" + " - " + .ResponseText)
            Exit Sub
        End If
        
    End With
    
    ' clean up
    Set xhr = Nothing
    
    Exit Sub
    
ErrorHandler:
    ' Check if the error is due to an invalid token
    If IsTokenError(Err.Description) And RetryCount < MaxRetries Then
        ' Invalidate the token and retry the API call
        RevolutToken = ""
        TokenExpiry = 0
        RetryCount = RetryCount + 1
        Resume StartCall
    Else
        ' Other errors or too many retries
        MsgBox "Error: " & Err.Description
    End If

End Sub

Function Text1_GotFocus()
    If Len(Form_revolut_COUNTERPARTY!jwkstxt) > 0 Then
        Form_revolut_COUNTERPARTY!jwkstxt = ""
    End If
End Function


Public Function getPartyDetails(name, accountno, iban, bic, createdb4, limit)
    
    Const FORM_NAME As String = "revolut_COUNTERPARTY"

    DoCmd.OpenForm FORM_NAME, , , , , acDialog

    'get the phonenumbers from the table
    If formIsOpen(FORM_NAME) Then
        name = Form_revolut_COUNTERPARTY!name
        accountno = Form_revolut_COUNTERPARTY!accountno
        iban = Form_revolut_COUNTERPARTY!iban
        bic = Form_revolut_COUNTERPARTY!bic
        createdb4 = Form_revolut_COUNTERPARTY!createdb4
        limit = Form_revolut_COUNTERPARTY!limit
        
        DoCmd.Close acForm, FORM_NAME

    End If
                
End Function

