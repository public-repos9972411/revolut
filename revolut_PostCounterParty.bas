Option Compare Database
Option Explicit
Public jsonPaylaod As String

Sub postCounterparty()
    Dim xhr As Object
    Dim i As Integer
    Dim RetryCount As Integer
    Dim MaxRetries As Integer: MaxRetries = 1
    
    ' handdle any token error requests
    On Error GoTo ErrorHandler
    
StartCall:
    ' get the token
    RevolutToken = GetToken()
    
    ' set the http object
    Set xhr = CreateObject("MSXML2.ServerXMLHTTP.6.0")

    'get the payload from the inputbox
    jsonPayload = getJsonPayload()
    
    ' set endppoint url
    Dim RevolutURL As String
    RevolutURL = REVOLUT_BASE_URL + "/api/1.0/counterparty"

    ' set the http object
    Set xhr = CreateObject("MSXML2.ServerXMLHTTP.6.0")

    With xhr
        .Open "POST", RevolutURL, False
        .setRequestHeader "Accept", "application/json"
        .setRequestHeader "Content-Type", "application/json"
        .setRequestHeader "Authorization", "Bearer " + RevolutToken
        .send jsonPayload
        
        If .Status = 200 Or .Status = 204 Or .Status = 201 Then
            MsgBox ("CounterParty has been successfully created at Revolut:" & .ResponseText)
        Else
            Debug.Print .ResponseText
            MsgBox ("Error" + " - " + .ResponseText)
            Exit Sub
        
        End If
        
    End With
    
    ' clean up
    Set xhr = Nothing
    
    Exit Sub
    
ErrorHandler:
    ' Check if the error is due to an invalid token
    If IsTokenError(Err.Description) And RetryCount < MaxRetries Then
        ' Invalidate the token and retry the API call
        RevolutToken = ""
        TokenExpiry = 0
        RetryCount = RetryCount + 1
        Resume StartCall
    Else
        ' Other errors or too many retries
        MsgBox "Error: " & Err.Description
    End If


End Sub

Function getJsonPayload()

    Const FORM_NAME As String = "revolut_COUNTERPARTY"
    
    DoCmd.OpenForm FORM_NAME, , , , , acDialog
    
    'get the payload from the textbox
    If formIsOpen(FORM_NAME) Then
        getJsonPayload = Form_revolut_COUNTERPARTY!jwkstxt
        Call Text1_GotFocus
        DoCmd.Close acForm, FORM_NAME
        
    End If
    
End Function

Function Text1_GotFocus()
    If Len(Form_revolut_COUNTERPARTY!jwkstxt) > 0 Then
        Form_revolut_COUNTERPARTY!jwkstxt = ""
    End If
End Function

