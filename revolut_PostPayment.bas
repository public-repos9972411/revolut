Option Compare Database
Option Explicit
Public jsonPayload As String

Sub postPayment()
    Dim xhr As Object
    Dim revolutCount As Integer
    Dim RetryCount As Integer
    Dim MaxRetries As Integer: MaxRetries = 1
    
    revolutCount = 0
    
    ' handdle any token error requests
    On Error GoTo ErrorHandler
    
StartCall:
    ' get the token
    RevolutToken = GetToken()

    'get the payload from the inputbox
    jsonPayload = getJsonPayload()
    
    ' set endppoint url
    Dim RevolutURL As String
    RevolutURL = REVOLUT_BASE_URL + "/api/1.0/pay"

    ' set the http object
    Set xhr = CreateObject("MSXML2.ServerXMLHTTP.6.0")

    With xhr
        .Open "POST", RevolutURL, False
        .setRequestHeader "Accept", "application/json"
        .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
        .setRequestHeader "Authorization", "Bearer " + RevolutToken
        .send jsonPayload
        
        If .Status = 200 Or .Status = 204 Or .Status = 201 Then
            ' parse the response
            Dim RevolutjsonText As String
            RevolutjsonText = .ResponseText

            Dim RevolutPaymentjson As Object
            Set RevolutPaymentjson = JsonConverter.ParseJson(RevolutjsonText)
            
            ' initialise database
            Dim db As DAO.Database
            Set db = CurrentDb()
            
            Dim rsRevolut As DAO.Recordset
            Set rsRevolut = db.OpenRecordset("RevolutPayments", dbOpenDynaset)
            
            With rsRevolut
                .AddNew
                ![id_] = RevolutPaymentjson("id")
                ![State] = RevolutPaymentjson("state")
                ![created_at] = RevolutPaymentjson("created_at")
                ![completed_at] = RevolutPaymentjson("completed_at")
                .Update
            End With
            MsgBox "Successfully sent payment"
        Else
            MsgBox ("Error" + " - " + .ResponseText)
            Exit Sub
        
        End If
        
    End With
    
    ' clean up
    Set xhr = Nothing

    Exit Sub
   
ErrorHandler:
    ' Check if the error is due to an invalid token
    If IsTokenError(Err.Description) And RetryCount < MaxRetries Then
        ' Invalidate the token and retry the API call
        RevolutToken = ""
        TokenExpiry = 0
        RetryCount = RetryCount + 1
        Resume StartCall
    Else
        ' Other errors or too many retries
        MsgBox "Error: " & Err.Description
    End If

End Sub

Function getJsonPayload()

    Const FORM_NAME As String = "revolut_PAYMENT"
    
    DoCmd.OpenForm FORM_NAME, , , , , acDialog
    
    'get the payload from the textbox
    If formIsOpen(FORM_NAME) Then
        getJsonPayload = Form_revolut_PAYMENT!jwkstxt
        Call Text1_GotFocus
        DoCmd.Close acForm, FORM_NAME
        
    End If
    
End Function

Function Text1_GotFocus()
    If Len(Form_revolut_PAYMENT!jwkstxt) > 0 Then
        Form_revolut_PAYMENT!jwkstxt = ""
    End If
End Function

