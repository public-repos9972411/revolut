# revolut



## Getting started

This is an example API developed in VBA that enables a company to send money to clients throught the revolut payment infrasture. This enables automed payment opportuinies.

***

## Name
Revolut Payment API project in VBA

## Description
As mentioned previously, instead of a company that wishes to make payment to clients manaully, you can use an API to do this by leveraging revolut's API to do this for your. The API here uses this framework to send money safely and speedily to clinets all over the world. This can be automated from creating counterparties to the actual sendoing of money using various currencies. 

## Visuals
I will visuals to this soon.

## Installation
This repo is to give you an idea of the flows involved but not to install directly. 

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
TIf you need any help please reached out by sending me an email to contact@ephostguru.com.

## Roadmap
There wouldn't be any future releases but feel free to fork it and improve on it.

## Authors and acknowledgment
I am the only author for this API.

## License
For open source projects, say how it is licensed.

## Project status
Feel free to step in and fork the project if you would like to improve it.