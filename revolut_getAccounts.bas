Option Compare Database
Option Explicit

Sub getAccounts()
    Dim xhr As Object

    Dim RetryCount As Integer
    Dim MaxRetries As Integer: MaxRetries = 1
    
    ' handdle any token error requests
    On Error GoTo ErrorHandler
    
StartCall:
    ' get the token
    RevolutToken = GetToken()
 
    ' set up the URI
    Dim RevolutCounterURL As String
    RevolutCounterURL = REVOLUT_BASE_URL + "/api/1.0/accounts"
    
    ' set the http object
    Set xhr = CreateObject("MSXML2.ServerXMLHTTP.6.0")

    With xhr
        .Open "GET", RevolutCounterURL, False
        .setRequestHeader "Accept", "application/json"
        .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
        .setRequestHeader "Authorization", "Bearer " + RevolutToken
        .send
        
        If .Status = 200 Or .Status = 204 Or .Status = 201 Then
            ' parse the response
            Dim RevolutjsonText As String
            RevolutjsonText = .ResponseText
            ' Debug.Print RevolutjsonText
            ' save date to table
            saveAccounts (RevolutjsonText)
        Else
            MsgBox ("Error" + " - " + .ResponseText)
            Exit Sub
        End If
        
    End With
    
    ' clean up
    Set xhr = Nothing
    
    Exit Sub
    
ErrorHandler:
    ' Check if the error is due to an invalid token
    If IsTokenError(Err.Description) And RetryCount < MaxRetries Then
        ' Invalidate the token and retry the API call
        RevolutToken = ""
        TokenExpiry = 0
        RetryCount = RetryCount + 1
        Resume StartCall
    Else
        ' Other errors or too many retries
        MsgBox "Error: " & Err.Description
    End If

End Sub

Function Text1_GotFocus()
    If Len(Form_revolut_COUNTERPARTY!jwkstxt) > 0 Then
        Form_revolut_COUNTERPARTY!jwkstxt = ""
    End If
End Function


Public Function getPartyDetails(name, accountno, iban, bic, createdb4, limit)
    
    Const FORM_NAME As String = "revolut_COUNTERPARTY"

    DoCmd.OpenForm FORM_NAME, , , , , acDialog

    'get the phonenumbers from the table
    If formIsOpen(FORM_NAME) Then
        name = Form_revolut_COUNTERPARTY!name
        accountno = Form_revolut_COUNTERPARTY!accountno
        iban = Form_revolut_COUNTERPARTY!iban
        bic = Form_revolut_COUNTERPARTY!bic
        createdb4 = Form_revolut_COUNTERPARTY!createdb4
        limit = Form_revolut_COUNTERPARTY!limit
        
        DoCmd.Close acForm, FORM_NAME

    End If
                
End Function

