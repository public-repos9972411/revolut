Option Compare Database
Option Explicit
Public Datafilters As DataInfo
        
Sub getTransactions()
    Dim xhr As Object
    Dim json As Object
    Dim RetryCount As Integer
    Dim MaxRetries As Integer: MaxRetries = 1
    
    ' handdle any token error requests
    On Error GoTo ErrorHandler
    
StartCall:
    ' get the token
    RevolutToken = GetToken()
    
    'get the date and mid, date must be in iso format
    Call getDatafilters(Datafilters)
    
    ' set up the URI
    Dim TransactionURL As String
    
    ' build the url based on available parameters if any
    TransactionURL = BuildURL(TransactionURL, Datafilters)
    
    ' set the http object
    Set xhr = CreateObject("MSXML2.ServerXMLHTTP.6.0")

    With xhr
        .Open "GET", TransactionURL, False
        .setRequestHeader "Accept", "application/json"
        .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
        .setRequestHeader "Authorization", "Bearer " + RevolutToken
        .send
        
        If .Status = 200 Or .Status = 204 Or .Status = 201 Then
        
            ' parse the response
            Dim RevolutjsonText As String
            RevolutjsonText = .ResponseText
'            Debug.Print RevolutjsonText
            
            Dim Transactionsjson As Object
            Set Transactionsjson = JsonConverter.ParseJson(RevolutjsonText)
            
            'save the transactions
            saveTransactions Transactionsjson
             
        Else
            MsgBox ("Error" + " - " + .ResponseText)
            Exit Sub
        End If
        
    End With
    
    ' clean up
    Set xhr = Nothing
    
    Exit Sub
    
ErrorHandler:
    ' Check if the error is due to an invalid token
    If IsTokenError(Err.Description) And RetryCount < MaxRetries Then
        ' Invalidate the token and retry the API call
        RevolutToken = ""
        TokenExpiry = 0
        RetryCount = RetryCount + 1
        Resume StartCall
    Else
        ' Other errors or too many retries
        MsgBox "Error: " & Err.Description
    End If
    
End Sub

